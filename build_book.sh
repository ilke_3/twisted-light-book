#!/bin/bash

book_folder=twisted-light-book 
toplevel=$(git rev-parse --show-toplevel)

pushd $toplevel

  # build the jupyter book
  jupyter-book build $book_folder

  # move the build artifacts to the public folder
  rm -rf public
  mv $book_folder/_build/html public
  chmod -R ugo+rX public
popd
